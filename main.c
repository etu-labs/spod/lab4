#include <stdio.h>
#include </usr/local/Cellar/open-mpi/4.1.4_2/include/mpi.h>
#include <time.h>
#include <stdlib.h>

int isEven(int number) {
    return number % 2 == 0;
}

double randomDouble(double min, double max) {
    double range = (max - min);
    double div = RAND_MAX / range;
    return min + (rand() / div);
}

void printArray(double arr[], int size) {
    printf("[ ");
    for (int i = 0; i < size; i++) {
        printf("%f ", arr[i]);
    }
    printf("]\n");
}

void fillArray(double* arr, int size) {
    for (int i = 0; i < size; i++) {
        double n = randomDouble(0, 10);
        arr[i] = n;
    }
}

int main(int argc, char* argv[]) {
    int num, rank;
    int arraySize = 3;
    int rootRank = 0;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    srand(clock() * rank);

    double arr[arraySize];

    if (isEven(rank)) {
        fillArray(arr, arraySize);
        printf("proc %d: ", rank);
        printArray(arr, arraySize);
    }

    double start = MPI_Wtime();

    double mins[arraySize];
    MPI_Comm comm;
    int color;

    if (isEven(rank)) {
        color = 1;
    } else {
        color = 2;
    }

    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &comm);
    MPI_Reduce(&arr, &mins, arraySize, MPI_DOUBLE, MPI_MIN, rootRank, comm);

    if (rank == rootRank) {
        printf("mins: ");
        printArray(mins, arraySize);
        printf("Total time: %f ms\n", (MPI_Wtime() - start)*1000);
    }

    MPI_Finalize();
    return 0;
}
